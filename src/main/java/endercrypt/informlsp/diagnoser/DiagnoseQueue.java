package endercrypt.informlsp.diagnoser;


import endercrypt.informlsp.server.documents.InformDocument;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;


public class DiagnoseQueue
{
	private final BlockingQueue<InformDocument> queue = new LinkedBlockingDeque<>();
	
	public synchronized void push(InformDocument document)
	{
		if (queue.contains(document) == false)
		{
			queue.add(document);
		}
	}
	
	/**
	 * Subject to race conditions!
	 */
	public boolean isEmpty()
	{
		return queue.isEmpty();
	}
	
	public InformDocument pull() throws InterruptedException
	{
		return queue.take();
	}
}
