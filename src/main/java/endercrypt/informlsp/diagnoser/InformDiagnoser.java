package endercrypt.informlsp.diagnoser;


import endercrypt.informlsp.compiler.InformCompiler;
import endercrypt.informlsp.server.documents.InformDocument;
import endercrypt.informlsp.utility.InformLspLogger;
import endercrypt.library.commons.misc.Ender;

import java.util.List;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.services.LanguageClient;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class InformDiagnoser
{
	public static InformDiagnoser launch()
	{
		InformDiagnoser diagnoser = new InformDiagnoser();
		diagnoser.thread.start();
		InformLspLogger.verbose("Thread started");
		return diagnoser;
	}
	
	private final DiagnoseQueue queue = new DiagnoseQueue();
	private final Thread thread = new Thread(this::thread);
	private final InformCompiler compiler = new InformCompiler();
	
	public void schedule(InformDocument document)
	{
		queue.push(document);
	}
	
	private void thread()
	{
		try
		{
			while (true)
			{
				processNext();
			}
		}
		catch (InterruptedException e)
		{
			InformLspLogger.verbose("Thread stopped");
		}
	}
	
	private void processNext() throws InterruptedException
	{
		try
		{
			if (queue.isEmpty())
			{
				InformLspLogger.verbose("Waiting for more edits to come through ...");
			}
			InformDocument document = queue.pull();
			InformLspLogger.info("Now diagnosing " + document.getUri());
			LanguageClient client = document.getClient();
			String uri = document.getUri();
			List<Diagnostic> diagnostics = document.performDiagnostic(compiler);
			client.publishDiagnostics(new PublishDiagnosticsParams(uri, diagnostics));
		}
		catch (InterruptedException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			InformLspLogger.error("failed to process next document\n" + Ender.exception.toString(e));
		}
	}
	
	public void stop()
	{
		thread.interrupt();
	}
}
