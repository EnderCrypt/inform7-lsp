package endercrypt.informlsp;


import endercrypt.informlsp.server.InformLanguageServer;
import endercrypt.informlsp.utility.InformLspLogger;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.launcher.PlainApplication;

import org.eclipse.lsp4j.jsonrpc.Launcher;
import org.eclipse.lsp4j.launch.LSPLauncher;
import org.eclipse.lsp4j.services.LanguageClient;


public class Main extends PlainApplication<Arguments>
{
	public static void main(String[] args)
	{
		PlainApplication.launch(Main.class, args);
	}
	
	@Override
	public void main(Arguments arguments, Information information) throws Exception
	{
		InformLspLogger.info("Inform7 LSP launched!");
		try
		{
			InformLanguageServer server = new InformLanguageServer();
			Launcher<LanguageClient> launcher = LSPLauncher.createServerLauncher(server, System.in, System.out);
			server.setClient(launcher.getRemoteProxy());
			launcher.startListening().get();
		}
		catch (Exception e)
		{
			InformLspLogger.error("Failed to start:\n" + Ender.exception.toString(e));
		}
	}
}
