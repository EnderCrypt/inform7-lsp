package endercrypt.informlsp.server;


import endercrypt.informlsp.server.documents.InformDocumentService;
import endercrypt.informlsp.utility.InformLspLogger;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.DidChangeConfigurationParams;
import org.eclipse.lsp4j.DidChangeWatchedFilesParams;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;


public class InformLanguageServer implements LanguageServer
{
	private final InformDocumentService documents = new InformDocumentService();
	
	@Override
	public CompletableFuture<InitializeResult> initialize(InitializeParams params)
	{
		return CompletableFuture.supplyAsync(new Supplier<InitializeResult>()
		{
			@Override
			public InitializeResult get()
			{
				ServerCapabilities capabilities = new ServerCapabilities();
				capabilities.setTextDocumentSync(TextDocumentSyncKind.Incremental);
				capabilities.setCodeActionProvider(true);
				capabilities.setCompletionProvider(new CompletionOptions(true, List.of()));
				
				return new InitializeResult(capabilities);
			}
		});
	}
	
	public void setClient(LanguageClient client)
	{
		documents.setClient(client);
	}
	
	@Override
	public CompletableFuture<Object> shutdown()
	{
		return CompletableFuture.completedFuture(null);
	}
	
	@Override
	public void exit()
	{
		System.exit(0);
	}
	
	@Override
	public TextDocumentService getTextDocumentService()
	{
		return documents;
	}
	
	@Override
	public WorkspaceService getWorkspaceService()
	{
		return new WorkspaceService()
		{
			@Override
			public void didChangeWatchedFiles(DidChangeWatchedFilesParams params)
			{
				InformLspLogger.warn("didChangeWatchedFiles " + params);
			}
			
			@Override
			public void didChangeConfiguration(DidChangeConfigurationParams params)
			{
				InformLspLogger.warn("didChangeConfiguration " + params);
			}
		};
	}
}
