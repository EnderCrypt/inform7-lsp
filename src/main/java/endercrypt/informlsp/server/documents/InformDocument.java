package endercrypt.informlsp.server.documents;


import endercrypt.informlsp.InformLspException;
import endercrypt.informlsp.compiler.InformCompiler;
import endercrypt.informlsp.compiler.result.errors.InformError;
import endercrypt.informlsp.utility.InformLspLogger;

import java.util.List;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.TextDocumentContentChangeEvent;
import org.eclipse.lsp4j.TextDocumentItem;
import org.eclipse.lsp4j.services.LanguageClient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;


@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class InformDocument
{
	public static InformDocument fromTextDocumentItem(LanguageClient client, TextDocumentItem item)
	{
		String uri = item.getUri();
		String contents = item.getText();
		return new InformDocument(client, uri, contents);
	}
	
	private final LanguageClient client;
	
	@EqualsAndHashCode.Include
	private final String uri;
	
	private final StringBuilder buffer;
	
	public InformDocument(@NonNull LanguageClient client, @NonNull String uri, @NonNull String contents)
	{
		this.client = client;
		this.uri = uri;
		this.buffer = new StringBuilder(contents);
	}
	
	protected void modify(TextDocumentContentChangeEvent changeEvent)
	{
		String text = changeEvent.getText();
		Range range = changeEvent.getRange();
		if (range == null)
		{
			buffer.setLength(0);
			buffer.append(text);
		}
		else
		{
			int start = range.getStart().getCharacter();
			int end = range.getEnd().getCharacter();
			InformLspLogger.verbose("Received edit on characters " + start + " to " + end);
			buffer.replace(start, end, text);
		}
	}
	
	public List<Diagnostic> performDiagnostic(InformCompiler compiler)
	{
		return compiler.execute(this)
			.getErrors()
			.stream()
			.map(InformError::createDiagnostic)
			.toList();
	}
	
	private int getLineIndex(int line)
	{
		int lineIndex = 0;
		for (int i = 0; i < line && lineIndex != -1; i++)
		{
			lineIndex = buffer.indexOf("\n", lineIndex);
			if (lineIndex > 0)
			{
				lineIndex++;
			}
		}
		if (lineIndex == buffer.length())
		{
			lineIndex = buffer.length() - 1;
		}
		return lineIndex;
	}
	
	public String getLine(int line)
	{
		int lineIndexStart = getLineIndex(line);
		if (lineIndexStart == -1)
		{
			throw new InformLspException("line number " + line + " does not exist");
		}
		int lineIndexEnd = buffer.indexOf("\n", lineIndexStart);
		if (lineIndexEnd == -1)
		{
			lineIndexEnd = buffer.length();
		}
		return buffer.substring(lineIndexStart, lineIndexEnd);
	}
	
	public Position getEndPosition()
	{
		int line = 0;
		int character = 0;
		for (char c : toString().toCharArray())
		{
			if (c == '\n')
			{
				line++;
			}
			else
			{
				character++;
			}
		}
		return new Position(line, character);
	}
	
	@Override
	public String toString()
	{
		return buffer.toString();
	}
}
