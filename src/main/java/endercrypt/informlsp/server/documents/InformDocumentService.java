package endercrypt.informlsp.server.documents;


import endercrypt.informlsp.diagnoser.InformDiagnoser;
import endercrypt.informlsp.utility.InformLspLogger;
import endercrypt.library.commons.misc.Ender;

import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.DidSaveTextDocumentParams;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.TextDocumentService;

import lombok.RequiredArgsConstructor;
import lombok.Setter;


@RequiredArgsConstructor
public class InformDocumentService implements TextDocumentService
{
	private final InformDocumentRepository documentRepository = new InformDocumentRepository();
	private final InformDiagnoser diagnoser = InformDiagnoser.launch();
	
	@Setter
	private LanguageClient client;
	
	@Override
	public void didOpen(DidOpenTextDocumentParams params)
	{
		InformDocument document = InformDocument.fromTextDocumentItem(client, params.getTextDocument());
		documentRepository.push(document);
	}
	
	@Override
	public void didChange(DidChangeTextDocumentParams params)
	{
		try
		{
			// update internal state
			String uri = params.getTextDocument().getUri();
			InformDocument document = documentRepository.require(uri);
			params.getContentChanges()
				.forEach(document::modify);
			
			// diagnostic
			diagnoser.schedule(document);
			
			// log
			InformLspLogger.info("Received edit on " + uri);
		}
		catch (Exception e)
		{
			InformLspLogger.error(Ender.exception.toString(e));
		}
	}
	
	@Override
	public void didClose(DidCloseTextDocumentParams params)
	{
		String uri = params.getTextDocument().getUri();
		documentRepository.delete(uri);
	}
	
	@Override
	public void didSave(DidSaveTextDocumentParams params)
	{
		// do nothing 
	}
}
