package endercrypt.informlsp.server.documents;


import endercrypt.informlsp.InformLspException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;


public class InformDocumentRepository implements Iterable<InformDocument>
{
	private final Map<String, InformDocument> documents = new HashMap<>();
	
	public void push(InformDocument document)
	{
		documents.put(document.getUri(), document);
	}
	
	public Optional<InformDocument> get(String uri)
	{
		return Optional.ofNullable(documents.get(uri));
	}
	
	public InformDocument require(String uri)
	{
		return get(uri)
			.orElseThrow(() -> new InformLspException("attempted to modify missing document " + uri));
	}
	
	public Optional<InformDocument> delete(String uri)
	{
		return Optional.ofNullable(documents.remove(uri));
	}
	
	@Override
	public Iterator<InformDocument> iterator()
	{
		return Collections.unmodifiableCollection(documents.values()).iterator();
	}
}
