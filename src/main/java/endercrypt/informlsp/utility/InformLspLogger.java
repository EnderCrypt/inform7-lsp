package endercrypt.informlsp.utility;


import endercrypt.informlsp.InformLspException;
import endercrypt.library.commons.JarInfo;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.tricks.StreamTricks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.TypeConversionException;


@RequiredArgsConstructor
@Getter
public class InformLspLogger
{
	public static final Path destination = JarInfo.workingDirectory.resolve("log.txt");
	private static Level level = Level.DEBUG;
	
	public static void debug(String message)
	{
		log(Level.DEBUG, message);
	}
	
	public static void verbose(String message)
	{
		log(Level.VERBOSE, message);
	}
	
	public static void info(String message)
	{
		log(Level.INFO, message);
	}
	
	public static void warn(String message)
	{
		log(Level.WARN, message);
	}
	
	public static void error(String message)
	{
		log(Level.ERROR, message);
	}
	
	public static void fatal(String message)
	{
		log(Level.FATAL, message);
	}
	
	private static void log(Level level, String message)
	{
		if (level.ordinal() < InformLspLogger.level.ordinal())
			return;
		String line = "[" + getLoggerName() + "/" + level + "] " + message + "\n";
		
		try
		{
			Files.write(destination, line.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		}
		catch (IOException e)
		{
			throw new InformLspException("Failed to log message", e);
		}
	}
	
	private static String getLoggerName()
	{
		for (StackTraceElement stackTraceElement : Ender.clazz.getStackTrace())
		{
			List<String> packageParts = new ArrayList<>(List.of(stackTraceElement.getClassName().split("\\.")));
			String className = packageParts.remove(packageParts.size() - 1);
			if (packageParts.contains("utility") == false)
			{
				return className;
			}
		}
		throw new InformLspException("Failed to find a suitable logger name");
	}
	
	public enum Level
	{
		DEBUG,
		VERBOSE,
		INFO,
		WARN,
		ERROR,
		FATAL;
	}
	
	public static class PicocliLevelConverter implements ITypeConverter<Level>
	{
		@Override
		public Level convert(String value) throws Exception
		{
			return Ender.parse.toEnum(Level.class, false, value)
				.orElseThrow(StreamTricks.throwWithMessage(TypeConversionException::new, "Available log level: " + Arrays.toString(Level.values())));
		}
	}
}
