package endercrypt.informlsp;

@SuppressWarnings("serial")
public class InformLspException extends RuntimeException
{
	public InformLspException(String message)
	{
		super(message);
	}
	
	public InformLspException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
