package endercrypt.informlsp.compiler.result.errors;


import endercrypt.informlsp.server.documents.InformDocument;
import endercrypt.informlsp.utility.InformLspLogger;

import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.DiagnosticSeverity;
import org.eclipse.lsp4j.Range;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public abstract class InformError
{
	public static InformError create(InformDocument document, String message)
	{
		return InformErrorWithLine.createWithLineNumber(document, message)
			.orElseGet(() -> new InformErrorGlobally(document, message));
	}
	
	private final InformDocument document;
	private final String message;
	
	public final Diagnostic createDiagnostic()
	{
		Range range = getRange();
		
		int startLine = range.getStart().getLine();
		boolean oneLine = startLine == range.getEnd().getLine();
		InformLspLogger.info("Scouted error " + (oneLine ? "on line " + startLine : "somewhere"));
		
		Diagnostic diagnostic = new Diagnostic();
		diagnostic.setSeverity(DiagnosticSeverity.Error);
		diagnostic.setMessage(getMessage());
		diagnostic.setRange(range);
		return diagnostic;
	}
	
	protected abstract Range getRange();
	
	@Override
	public final String toString()
	{
		return getClass() + "[\"" + getMessage() + "\"]";
	}
}
