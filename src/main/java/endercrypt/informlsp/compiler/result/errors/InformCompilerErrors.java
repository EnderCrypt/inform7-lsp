package endercrypt.informlsp.compiler.result.errors;


import endercrypt.informlsp.server.documents.InformDocument;
import endercrypt.library.commons.pubimm.PublicallyImmutableList;

import lombok.RequiredArgsConstructor;


@SuppressWarnings("serial")
public class InformCompilerErrors extends PublicallyImmutableList<InformError>
{
	public InformCompilerErrors(InformDocument document, String compilerOutput)
	{
		InformErrorBuilder builder = new InformErrorBuilder(document);
		for (String line : compilerOutput.split("\n"))
		{
			if (builder.push(line) == false && builder.hasText())
			{
				elements.add(builder.siphonError());
				builder.push(line);
			}
		}
		if (builder.hasText())
		{
			elements.add(builder.siphonError());
		}
	}
	
	@RequiredArgsConstructor
	private static class InformErrorBuilder
	{
		private static final String expectedStart = "  >--> ";
		private static final String expectedSpaces = "    ";
		
		private final StringBuilder builder = new StringBuilder();
		private final InformDocument document;
		
		public boolean hasText()
		{
			return builder.length() > 0;
		}
		
		public InformError siphonError()
		{
			InformError error = InformError.create(document, toString());
			reset();
			return error;
		}
		
		private void reset()
		{
			builder.setLength(0);
		}
		
		public boolean push(String line)
		{
			if (line.startsWith(expectedStart))
			{
				if (hasText())
				{
					return false;
				}
				line = expectedSpaces + line.substring(expectedStart.length());
			}
			
			return consumeLine(line);
		}
		
		private boolean consumeLine(String line)
		{
			if (line.startsWith(expectedSpaces) == false)
			{
				return false;
			}
			line = line.substring(expectedSpaces.length());
			builder.append(line).append("\n");
			return true;
		}
		
		@Override
		public String toString()
		{
			return builder.toString();
		}
	}
}
