package endercrypt.informlsp.compiler.result.errors;


import endercrypt.informlsp.server.documents.InformDocument;
import endercrypt.informlsp.utility.InformLspLogger;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

import lombok.Getter;


@Getter
public class InformErrorWithLine extends InformError
{
	private static final Pattern lineRegex = Pattern.compile("\\(source text, line (\\d+)\\)");
	
	protected static Optional<InformError> createWithLineNumber(InformDocument document, String message)
	{
		Matcher lineMatcher = lineRegex.matcher(message.replace('\n', ' '));
		if (lineMatcher.find() == false)
			return Optional.empty();
		int lineNumber = Integer.parseInt(lineMatcher.group(1)) - 1;
		return Optional.of((InformError) new InformErrorWithLine(document, message, lineNumber));
	}
	
	private final int lineNumber;
	
	private InformErrorWithLine(InformDocument document, String message, int lineNumber)
	{
		super(document, message);
		this.lineNumber = lineNumber;
	}
	
	@Override
	protected Range getRange()
	{
		int lineNumber = getLineNumber();
		InformLspLogger.info("Scouted error on line " + lineNumber);
		String line = getDocument().getLine(lineNumber);
		Position start = new Position(lineNumber, 0);
		Position end = new Position(lineNumber, line.length());
		return new Range(start, end);
	}
}
