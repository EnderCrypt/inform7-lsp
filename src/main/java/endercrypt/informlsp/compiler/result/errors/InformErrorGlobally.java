package endercrypt.informlsp.compiler.result.errors;


import endercrypt.informlsp.server.documents.InformDocument;

import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;


public class InformErrorGlobally extends InformError
{
	public InformErrorGlobally(InformDocument document, String message)
	{
		super(document, message);
	}
	
	@Override
	protected Range getRange()
	{
		Position start = new Position(0, 0);
		Position end = new Position(0, 0);
		
		// Range is required, but highlighting whole file looks ugly and distracting
		// Position end = getDocument().getEndPosition();
		
		return new Range(start, end);
	}
}
