package endercrypt.informlsp.compiler.result;


import endercrypt.informlsp.compiler.result.errors.InformCompilerErrors;
import endercrypt.informlsp.server.documents.InformDocument;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class InformCompilationResult
{
	public static InformCompilationResult parse(InformDocument document, String compilerOutput)
	{
		InformCompilerErrors errors = new InformCompilerErrors(document, compilerOutput);
		return new InformCompilationResult(errors);
	}
	
	private final InformCompilerErrors errors;
}
