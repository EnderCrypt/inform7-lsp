package endercrypt.informlsp.compiler;


import endercrypt.informlsp.InformLspException;
import endercrypt.informlsp.compiler.result.InformCompilationResult;
import endercrypt.informlsp.server.documents.InformDocument;
import endercrypt.informlsp.utility.InformLspLogger;
import endercrypt.library.commons.JarInfo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;


public class InformCompiler
{
	private final Path binary;
	
	public InformCompiler()
	{
		binary = JarInfo.workingDirectory.resolve("compile.sh");
	}
	
	public synchronized InformCompilationResult execute(InformDocument document)
	{
		Process process = null;
		try
		{
			ProcessBuilder builder = new ProcessBuilder(binary.toString());
			process = builder.start();
			InformLspLogger.info("Launched compiler with pid " + process.pid());
			try (OutputStream output = process.getOutputStream())
			{
				String story = document.toString();
				output.write(story.getBytes());
			}
			try (InputStream input = process.getErrorStream())
			{
				String response = new String(input.readAllBytes());
				InformLspLogger.verbose("process response: \n" + response);
				return InformCompilationResult.parse(document, response);
			}
		}
		catch (IOException e)
		{
			throw new InformLspException("Failed to run compiler", e);
		}
		finally
		{
			InformLspLogger.info("Compiler finished " + getProcessExitMessage(process));
		}
	}
	
	private static String getProcessExitMessage(Process process)
	{
		if (process == null)
		{
			return "whitout ever having spawned";
		}
		if (process.isAlive())
		{
			return "whitout having exited";
		}
		return "with exit value " + process.exitValue();
	}
}
