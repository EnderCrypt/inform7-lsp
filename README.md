# Inform7 LSP Server

This is a functional, albeit perhaps a bit janky.

I wrote this mainly for [Vim](https://www.vim.org/) and so have only tested it there using [ALE](https://github.com/dense-analysis/ale):

Add this to ~/.vimrc
```vim
let g:ale_linters = {'inform': ['Inform 7']}

call ale#linter#Define('inform', {
\       'name': 'Inform 7',
\       'lsp': 'stdio',
\       'executable': '/path/to/script/which/launches/inform-lsp-server.jar',
\       'command': '%e',
\       'project_root': '%',
\       })
autocmd BufNewFile,BufRead *.ni set filetype=inform
```

Execute this command while editing your inform7 file to trigger autocompletion:
```vim
:set filetype=inform
```

