#!/usr/bin/env bash

set -e

INPUT="/tmp/inform-lsp-input.ni"
OUTPUT="/tmp/inform-lsp-output.i6"

SOURCE="$(cat -)"
echo "$SOURCE" > "$INPUT"


ni -no-census-update \
	-internal /usr/lib/inform7 \
	-external ~/.local/share/Inform \
	-format=inform6/16d \
	-o "$OUTPUT" "$INPUT"
	
